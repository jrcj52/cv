export const contactInfo = {
  name: 'Juan Raymundo Carrillo Jasso',
  photo: 'avatar.jpg',
  job: 'Web developer',
  email: 'jrcj52@gmail.com',
  phone: '8112726061',
};

export const languages = [
  {
    name: 'Spanish',
    display: 'Native',
    value: 10,
  },
  {
    name: 'English',
    display: '30% (non-conversational)',
    value: 3,
  },
];

export const jobs = [
  {
    from: '2016',
    to: '2018',
    job: 'Fullstack web developer',
    description: 'I worked at Castelec Internacional developing Sai Móvil (web), I developed over legacy code with Asp forms + jQuery and I implemented Vue js in some modules',
  },
  {
    from: '2018',
    to: '2021',
    job: 'Frontend React developer',
    description: 'I worked at Reservamos web funnel and its white labels like Greyhound, Primera Plus and Grupo Vencedor. My main job was develop in React and make solutions'
  },
  {
    from: '2021',
    to: '2022',
    job: 'Full stack js developer lead',
    description: `I developed a brandable web funnel for Intelimotor at Botware. Intelimotor the first software made to automate the comercialization of cars, this is used by vehicle agencies, I made some web components with Svelte.js`,
  },
  {
    from: '2022',
    to: 'Now',
    job: 'Frontend React developer',
    description: 'Currently I am dedicated to implementing new technologies, adding improvements and doing maintenance, another of my activities is to make the team apply good practices',
  }
];

export const academicTraining = [
  {
    from: '2009',
    to: '2012',
    school: 'Conalep Monterrey III',
    title: 'Profecional tecnico bachiller en informatica',
  },
  {
    from: '2012',
    to: '2016',
    school: 'Facultad De Ciencias Físico Matemáticas',
    title: 'Lic. en multimedia y animación digital'
  },
];

export const skills = [
  {
    icon: 'devicon-typescript-plain',
    name: 'Typescript',
  },
  {
    icon: 'devicon-javascript-plain',
    name: 'Javascript',
  },
  {
    icon: 'devicon-nodejs-plain',
    name: 'Node'
  },
  {
    icon: 'devicon-html5-plain',
    name: 'HTML5',
  },
  {
    icon: 'devicon-react-original',
    name: 'React',
  },
  {
    icon: 'devicon-devicon-plain',
    name: 'Svelte',
  },
  {
    icon: 'devicon-vuejs-plain',
    name: 'Vuejs'
  },
  {
    icon: 'devicon-sass-original',
    name: 'Sass',
  },
  {
    icon: 'devicon-jquery-plain',
    name: 'jQuery',
  },
  {
    icon: 'devicon-csharp-plain',
    name: 'C#'
  },
  {
    icon: 'devicon-postgresql-plain',
    name: 'PostgreSQL'
  },
  {
    icon: 'devicon-github-plain',
    name: 'Github',
  },
  {
    icon: 'devicon-gitlab-plain',
    name: 'gitlab',
  },
  {
    icon: 'devicon-digitalocean-plain',
    name: 'App platform',
  },
  {
    icon: 'devicon-heroku-original',
    name: 'Heroku',
  },
  {
    icon: 'devicon-javascript-plain',
    name: 'Stitches'
  },
  {
    icon: 'devicon-tailwindcss-original-wordmark',
    name: 'Tailwind'
  },
  {
    icon: 'devicon-tailwindcss-original-wordmark',
    name: 'Windi'
  },
];

export const projects = [
  {
    icon: 'devicon-nodejs-plain',
    name: 'Schematizer',
    description: 'Is a public npm library to make graphql Schemas with typescript',
    url: 'https://www.npmjs.com/package/@schematizer/schematizer',
  },
  {
    icon: 'devicon-nodejs-plain',
    name: 'Schematizer Auth',
    description: 'Is a public npm library, authorization plugin for Schematizer',
    url: 'https://www.npmjs.com/package/@schematizer/auth',
  },
  {
    icon: 'devicon-svelte-plain',
    name: 'This page',
    description: 'Personal CV made with Svelte and Windi CSS',
    url: 'https://gitlab.com/jrcj52/cv/',
  },
  {
    icon: 'devicon-typescript-plain',
    name: 'Coffeetch',
    description: 'Fetch wrapper utility',
    url: 'https://gitlab.com/ray-bucket/coffeetch',
  },
  {
    icon: 'devicon-nodejs-plain',
    name: 'Web Health Bot',
    description: 'Telegram bot to program http request ',
    url: 'https://gitlab.com/ray-bucket/web-health-bot',
  },
  {
    icon: 'devicon-arduino-plain',
    name: 'ESP32 Bluetooth Car',
    description: 'Code to make a rc car',
    url: 'https://gitlab.com/ray-bucket/esp32-bluetooth-car-v1'
  },
  {
    icon: 'devicon-arduino-plain',
    name: 'ESP32 Water flow',
    description: 'ESP32 as waterflow sensor',
    url: 'https://gitlab.com/ray-bucket/esp32-water-flow',
  },
  {
    icon: 'devicon-svelte-plain',
    name: 'Options Selector',
    description: 'Select component without UI make with svelte',
    url: 'https://www.npmjs.com/package/@slotvelte/options-selector',
  },
  {
    icon: 'devicon-javascript-plain',
    name: 'Hexmap',
    description: 'hexagon map library with functions for searching, selection and another algorithms',
    url: 'https://gitlab.com/pubic/hexmap/-/tree/setup',
  },
];
